AUX = code.aux copying.aux influence.aux introduction.aux logistique.aux narlarp.aux theme.aux securite.aux personnage.aux
MAIN = narlarp

all: pdf

pdf: dvi
	dvipdf $(MAIN).dvi

dvi:
	latex $(MAIN).tex && latex $(MAIN).tex
clean:
	rm $(MAIN).pdf $(MAIN).log $(MAIN).dvi $(AUX)
